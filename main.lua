#! /bin/sh

local args = {...}
local utf8, ansicolors
local string, options, argHandling = {__BC = _G["string"]}, {}, {}

local patterns, sampleGroups
local patternFile, sampleFiles

local helpText = "Coming soon\n"

setmetatable(string, {__index = function(s, k) return s.__BC[k] end})

if not pcall(require, "lua-utf8") then
	io.stderr:write("** UTF8 support disabled, install 'luautf8' from luarocks to enable it. **\n")
else
	utf8 = require("lua-utf8")
	
	-- Adding utf8 functions into string
	for k, v in pairs(utf8) do
		string[k] = v
	end
end

if not pcall(require, "ansicolors") then
	io.stderr:write("** Colors support disabled, install 'ansicolors' from luarocks to enable them. **\n")
else
	ansicolors = require("ansicolors")
end

options["DOT_NEWLINE"] = false
options["COLORS"] = ansicolors and true
options["SHOW_OFFSET"] = false

argHandling["-d"] = function(args, i) options["DOT_NEWLINE"] = true end
argHandling["-c"] = function(args, i) options["COLORS"] = false end
argHandling["-o"] = function(args, i) options["SHOW_OFFSET"] = true end
argHandling["-p"] = function(args, i)	
	local filepath = args[i+1]

	if not filepath then
		io.stdout:write("No pattern file was passed.\n")
		os.exit(0)
	end

	local f = io.open(filepath, "r")

	if not f then
		io.stdout:write("Error opening file '"..filepath.."'.\n")
		os.exit(0)
	end
	
	f:close()
	patternFile = filepath
	
	return 1
end
argHandling["-s"] = function(args, i)	
	local filepath, f
	
	sampleFiles = {}

	for j = i+1, #args do
		filepath = args[j] or ""
		if filepath:sub(1,1) == "-" then
			break
		end

		f = io.open(filepath, "r")
		if not f then
			io.stdout:write("Error opening file '"..filepath.."'.\n")
			os.exit(0)
		end
		f:close()

		table.insert( sampleFiles, filepath )
	end
	
	if #sampleFiles <= 0 then
		io.stdout:write("No sample files were passed.\n")
		os.exit(0)
	end

	return #sampleFiles + 1
end
argHandling["-h"] = function(args, i)	io.stdout:write(helpText)
	os.exit(0)
end

-- Handle arguments
do
	local i, k = 1 
	while i	<= #args do
		if argHandling[ args[i] ] then
			k = argHandling[ args[i] ](args, i)
		end
		i = i + (k or 1)
	end
end

-- Open pattern and sample files and read their contents
do
	if not patternFile or not sampleFiles then
		io.stdout:write("Missing pattern files or sample files.\n")
		os.exit(0)
	end
	
	patterns, sampleGroups = {}, {}

	local fileptr, tmp
	for i = 1, #sampleFiles do
		fileptr = io.open(sampleFiles[i], "r")
		tmp = {filename = sampleFiles[i], list = {}} 

		if options["DOT_NEWLINE"] then
			table.insert( tmp.list, fileptr:read("*a") )
		else
			for line in fileptr:lines() do	
				table.insert( tmp.list, line )
			end
		end

		fileptr:close()
		table.insert( sampleGroups, tmp )
	end
	
	fileptr = io.open(patternFile, "r")
	for line in fileptr:lines() do	
		table.insert( patterns, line )
	end
	fileptr:close()
end

do
	local sections
	local skipFirstNewline = true

	for patternIndex, singlePattern in ipairs(patterns) do
		sections = {}
		for sampleGroupIndex, sampleGroupContent in ipairs(sampleGroups) do
			sections[sampleGroupIndex] = {}

			for sampleIndex, singleSample in ipairs(sampleGroupContent.list) do
				sections[sampleGroupIndex][sampleIndex] = { string.match( singleSample, singlePattern ) }

				if options["SHOW_OFFSET"] then
					local start, finish, indexOffset = 1, 1, 1
					for sectionIndex, sectionContent in ipairs(sections[sampleGroupIndex][sampleIndex]) do
						start, finish = string.find( singleSample, sectionContent, indexOffset )
						sections[sampleGroupIndex][sampleIndex][sectionIndex] = 
							"%{red}(%{reset}"..start..","..finish
							.."%{red})%{magenta}{%{reset}"..(sectionContent or "")
							.."%{magenta}}%{reset}"
					indexOffset = finish + 1
					end

				else
					for sectionIndex, sectionContent in ipairs(sections[sampleGroupIndex][sampleIndex]) do
						sections[sampleGroupIndex][sampleIndex][sectionIndex] = 
							"%{magenta}{%{reset}"..(sectionContent or "").."%{magenta}}%{reset}"
					end
				end

				if #sections[sampleGroupIndex][sampleIndex] <= 0 then
					table.insert( sections[sampleGroupIndex][sampleIndex], 
						"%{magenta}*%{reset}No results%{magenta}*%{reset}"
					)
				end

				sections[sampleGroupIndex][sampleIndex] = table.concat({
					string.format(" %4d ", sampleIndex),
					table.concat(sections[sampleGroupIndex][sampleIndex], "\t")
				})
			end
			sections[sampleGroupIndex] = table.concat({
				"**%{bright}SampleFile: %{reset black greenbg}" .. sampleGroupContent.filename .. "%{reset}\n",
				table.concat(sections[sampleGroupIndex], "\n")
			})
		end
		
		local outputBuffer = table.concat{
			(skipFirstNewline and "" or "\n"), 
			"%{bright}Pattern: %{reset black greenbg}" .. singlePattern .. "%{reset}\n",
			table.concat(sections, "\n")
		}

		if not options["COLORS"] then
			outputBuffer = string.gsub( outputBuffer, "%%{.-}", "" )
		else
			outputBuffer = ansicolors( outputBuffer )
		end

		io.stdout:write( outputBuffer )
		io.stdout:write( "\n" )

		skipFirstNewline = false
	end
	
	io.stdout:flush()
end


