Lua pattern tester
==================

This program compares some text against **Lua patterns**,
much like online regex playgrounds.


Usage
-----

Argument `-p` expects one filename, the filename of the file containing the
patterns to use, and argument `-s` expects **one or more** filenames separated
by space, the files containing the sample text to match against, e.g:

	lua main.lua -p testing_patterns.txt -s students.txt teachers.txt parents.txt


There are other arguments that may be useful when testing **Lua patterns**:

`-o`  Shows the offsets of each match from the start of it. (1-based offset)

`-c`  Disables colored output, useful for piping with other programs.

`-h`  Shows the help. (not working yet, sorry)

